package BeSpaceDCore.SpatioTemporal

/**
 * Created by keith on 11/01/16.
 */

import BeSpaceDCore._
import BeSpaceDCore.Log._


class MapTimeSpec extends UnitSpec {

  val core = standardDefinitions; import core._

  // Time
  val t1 = new IntegerTime(1)
  val t2 = new IntegerTime(2)
  val t3 = new IntegerTime(3)
  val t4 = new IntegerTime(4)

  def calculateArea(box: OccupyBox): Int = {
    Math.abs((box.x2 - box.x1 + 1) * (box.y2 - box.y1 + 1))
  }

  "Spatio-Temporal Operator: mapTime" should "be able to map over time with a simple example" in {
    // Overlapping Boxes
    val b1 = OccupyBox(1, 1, 10, 10) // Area 100
    val b2 = OccupyBox(5, 5, 15, 15) // Area 121
    val b3 = OccupyBox(10, 10, 20, 20) // Area 121

    // Time occupied
    val to1 = IMPLIES(TimePoint(t1), b1)
    val to2 = IMPLIES(TimePoint(t2), b2)
    val to3 = IMPLIES(TimePoint(t3), b3)

    val timeSeries = List(to1, to2, to3)
    val conjunction = BIGAND(timeSeries)
    val initialValue = 0

    def areaOccupied(item: Invariant): ComponentState[Int] =
    {
      val area = item match {
        case box: OccupyBox => calculateArea(box)
        case _ => 0
      }

      testOff {
        println(s"time addAreaOccupied()...")
        println(s"              item = $item")
        println(s"              area = $area")
      }

      ComponentState(area)
    }

    val mappedTime = mapTime[ComponentState[Int]](areaOccupied)(conjunction)

    val expectedAreas = BIGAND(List(
      IMPLIES(TimePoint(t1), ComponentState(100)),
      IMPLIES(TimePoint(t2), ComponentState(121)),
      IMPLIES(TimePoint(t3), ComponentState(121))
    ))

    assertResult(expected = expectedAreas)(actual = mappedTime)
  }
  
  
  "Spatio-Temporal Operator: filterMapTime"  should "be able to map over time with a simple example" in {
    // Overlapping Boxes
    val b1 = OccupyBox(1, 1, 10, 10) // Area 100
    val b2 = OccupyBox(5, 5, 15, 15) // Area 121
    val b3 = OccupyBox(10, 10, 20, 20) // Area 121

    // Time occupied
    val to1 = IMPLIES(TimePoint(t1), b1)
    val to2 = IMPLIES(TimePoint(t2), b2)
    val to3 = IMPLIES(TimePoint(t3), b3)

    val timeSeries = List(to1, to2, to3)
    val conjunction = BIGAND(timeSeries)
    val initialValue = 0

    def areaOccupied(item: Invariant): ComponentState[Int] =
    {
      val area = item match {
        case box: OccupyBox => calculateArea(box)
        case _ => 0
      }

      testOff {
        println(s"time addAreaOccupied()...")
        println(s"              item = $item")
        println(s"              area = $area")
      }

      ComponentState(area)
    }

    val filteredMappedTime = filterMapTime[ComponentState[Int]](t1, t4, 1, TRUE())(areaOccupied)(conjunction)

    val expectedAreas = BIGAND(List(
      IMPLIES(TimePoint(t1), ComponentState(100)),
      IMPLIES(TimePoint(t2), ComponentState(121)),
      IMPLIES(TimePoint(t3), ComponentState(121))
    ))

    assertResult(expected = expectedAreas)(actual = filteredMappedTime)
  }

}


