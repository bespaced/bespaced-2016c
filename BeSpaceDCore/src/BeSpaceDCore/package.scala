import org.scalatest._

import scala.util.Either

/**
 * @author Keith Foster
 */

package object BeSpaceDCore {


  //-------------------------------- Standard Package includes

  import HazyTypes._



  //-------------------------------- Global Definitions
  private
  val CoreDefinitions = new CoreDefinitions()

  private
  val GraphOperations = new GraphOperations()



  //-------------------------------- Inversion of Control

  //TODO: Add some factory functions for core definitions here
  //      A factory function should be passed as a parameter to Apps and test suites
  //      so they can instantiate a core definition object to use.

  def standardDefinitions = CoreDefinitions



  // ------------------------------- Obsolete

  // Flatten and Unwrap the terms of conjunctions

  @Deprecated
  def flattenAnds(presumedAnd: Invariant): Invariant = CoreDefinitions.simplifyInvariant(BIGAND(unwrapAnds(presumedAnd)))

  @Deprecated
  def unwrapAnds(presumedAnd: Invariant): List[Invariant] =
  {
    presumedAnd match
    {
      case AND(t1, t2)                   => unwrapAnds(t1) ++ unwrapAnds(t2)
      case BIGAND(list: List[Invariant]) => list flatMap unwrapAnds
      case _                             => List(presumedAnd)
    }
  }



  //-------------------------------- Scala Language Improvements

  // Union Types
  type or[L, R] = Either[L, R] // Move this to a common module
  implicit def l2Or[L, R](l: L): L or R = Left(l)
  implicit def r2Or[L, R](r: R): L or R = Right(r)



  //-------------------------------- ScalaTest Base classes
  
  // Unit Testing
  abstract class UnitSpec extends FlatSpec with Matchers with OptionValues with Inside with Inspectors

}