/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */

/*
 *  J. O. Blech 2015
 * 
 *  Extracted from CoreDefinitions Feb 2015
 *
 *  licensed under Apache 2.0 (  )
 */

package BeSpaceDCore

import scala.math
import scala.math.PartiallyOrdered


// --------------------------------------------------- Time

trait Time2[+ConcreteTime] extends PartiallyOrdered[ConcreteTime]

// Natural Time

case class TotalTimeOrdered[S](symbol: S)(implicit ev: S => Ordering[S]) extends Ordered[TotalTimeOrdered[S]]
{
  override def compare(that: TotalTimeOrdered[S]): Int = this.symbol.compare(this.symbol, that.symbol)
}


// Event Relative Time

/*
 * Time Points may or may not be chosen.
 * Event relative time points are used to describe space/time behavior relative to an event
 */
abstract class ERTP extends Time2[ERTP]; // Event Relative Time Point

class TERTP[E, T](val event: E, val offset: T)(implicit ev: T => Ordering[T]) extends ERTP
{
  override def tryCompareTo [B >: ERTP](that: B)(implicit ev1: B => PartiallyOrdered[B]): Option[Int] =
  {
    that match {
      case tertp: TERTP[E, T] =>
        if (this.event != tertp.event)
        {
          None
        }
        else {
          Some(tertp.offset.compare(tertp.offset, this.offset))
        }
      case _ => None
    }
  }
}

//object TERTP {
//  def apply[E, T]( event: E,  offset: T)(implicit ev: T => Ordering[T]): TERTP[E, T] = { new TERTP[E, T](event, offset) }
//}

case class IntERTP[E](override val event: E, override val offset : Int) extends TERTP[E, Int](event, offset)(ev = { i:Int => Ordering[Int] })



// ------------------------------------------------- Symbolic Int

abstract class SI;
case class SI_C (c:Int) extends SI;
case class SI_Add (i1 : SI, i2 : SI) extends SI;
case class SI_Sub (i1 : SI, i2 : SI) extends SI;
case class SI_Times (i1 : SI, i2 :SI) extends SI;
case class SI_Var [V](v : V) extends SI



/*
 * Earth Geo Coordinates
 * added Jul 2015
 */

case class LatLong (latdegs : Int, latmins : Int, latsecs : Int, longdegs: Int, longmins : Int, longsecs : Int) // Latitude, Longitude coordinates



/*
 * Our Invariants
*/

abstract class Invariant extends Ordered[Invariant]
{
  def compare(that: Invariant) = this.toString.compare(that.toString)
} 

abstract class ATOM extends Invariant

case class OR (t1 : Invariant, t2 : Invariant) extends Invariant
case class AND (t1 : Invariant, t2 : Invariant)  extends Invariant
case class NOT (t : Invariant) extends Invariant
case class IMPLIES[P <: Invariant, C <: Invariant] (premise : P, conclusion : C)  extends Invariant

case class BIGOR[E <: Invariant]  (terms : List[E]) extends Invariant
case class BIGAND[E <: Invariant] (terms : List[E]) extends Invariant
object BIGAND { def apply[E <: Invariant](terms: E*) = { new BIGAND[E](terms.toList)} }

case class IntTimeStamp (timestamp : Int) extends ATOM
case class TimeStamp [T] (timestamp : T) extends ATOM // the generalized version of IntTimeStamp; new terminology: time point?
case class TimePoint [T] (timepoint : T) extends ATOM // new terminology, but not yet implemented 04/02/2014 !

case class TimePeriod [T](timestamp1 : T, timestamp2 : T) extends ATOM // new terminology: time interval?
case class TimeInterval [T](timepoint1 : T, timepoint2 : T) extends ATOM // new terminology, but not yet implemented 04/02/2014 !

//ways to specify time for time interval and time point

abstract class Time;
case class TStandardGMTDay (hours : Int, minutes : Int, seconds : Int) extends Time; 



// These should appear in clauses : Event /\ Owner /\ Prob ... -> Space
case class Event[E] (event : E) extends ATOM
case class Owner[O] (owner : O) extends ATOM { override def toString = s"Owner2(${owner.toString})"}
case class Prob (probability : Double) extends ATOM //probability 
case class ComponentState[S] (state : S) extends ATOM

//Geometric constructs
case class OccupyBox (x1 : Int,y1 : Int,x2 : Int,y2 : Int) extends ATOM
case class OccupyBoxSI (x1 : SI,y1 : SI,x2 : SI,y2 : SI) extends ATOM // symbolic coordinates, alternative concept to EROccupyBox
case class EROccupyBox (x1 : (ERTP => Int),y1 : (ERTP => Int),x2 : (ERTP => Int),y2 : (ERTP => Int)) extends ATOM // Coordinates depending on an event relative time point
case class OwnBox[C] (owningcomponent : C,x1 : Int,y1 : Int,x2 : Int,y2 : Int) extends ATOM // a box that is owned by a component. Both OccupyBox (for collision) and OwnBox (for identification in communitcation) are needed.
//infinitely large boxes
case class OccupyInfiniteBox () extends ATOM 
case class OwnInfiniteBox[C] (owningcomponent : C) extends ATOM


case class OccupyFreeBox (x1 : Int,y1 : Int,x2 : Int,y2 : Int, x3 : Int,y3 : Int,x4 : Int,y4 : Int) extends ATOM
case class Occupy3DBox (x1 : Int, y1: Int, z1 : Int, x2 : Int, y2 : Int, z2 : Int) extends ATOM
case class CommRadius (x :Int, y :Int, r : Int) extends ATOM

case class OccupyPoint (x:Int, y:Int) extends ATOM
case class OwnPoint[C] (owningcomponent : C,x:Int, y:Int) extends ATOM

case class Occupy3DPoint (x:Int, y:Int, z: Int) extends ATOM

case class OccupySegment (x1 : Int, y1 : Int, x2 : Int, y2 :Int, radius : Int) extends ATOM
case class OccupySegment3D (x1 : Int, y1 : Int, z1 : Int, x2 : Int, y2 :Int, z2 : Int, radius : Int) extends ATOM
case class OwnSegment[C] (owningcomponent : C, x1 : Int, y1 : Int, x2 : Int, y2 :Int, radius : Int) extends ATOM

case class OccupyCircle (x1 : Int, y1 : Int, radius : Int) extends ATOM // a 2D circle
case class OwnCircle[C] (owningcomponent : C, x1 : Int, y1 : Int, radius : Int) extends ATOM

//Topological constructs

case class OccupyNode[N] (node : N) extends ATOM
case class OwnNode[C,N](owningcomponent : C, node : N) extends ATOM
case class BetweenNodes[N](source : N, target : N) extends ATOM // for describing that a component is in transition between two nodes
case class Edge[N] (source : N, target : N) extends ATOM //for describing graph topological conditions conditions like: IF Edge THEN ...
case class Transition[N,E] (source : N, even : E, target : N) extends ATOM //for describing state transitions


// TODO: Change TRUE and FALSE classes to singleton objects and remove () from all references.
abstract class BOOL extends ATOM
{
  def not: BOOL = if(this == TRUE()) FALSE() else TRUE()
  def ! = not _
}
case class TRUE() extends BOOL
case class FALSE() extends BOOL

//Probabilistic constructs

case class PROBIMPLIES (probability: Double, t1 : Invariant, t2 : Invariant)  extends Invariant; // t1 implies t2 with a probability

