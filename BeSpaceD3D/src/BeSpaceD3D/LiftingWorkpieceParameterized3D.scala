
/*
* Jan Olaf Blech 
* RMIT University
* 2013
*/

/* needs runtime setting -Xss515m or similar, Sat4j has problems with number of variables, encodeAsSat uses to low range for encoding of x,y variables */


// 13/01/2013: needs fixing and second parameter

package BeSpaceD3D
import BeSpaceDCore._;
import java.awt.Rectangle
//import java.util.Timer
object LiftingWorkpieceParameterized3D extends CoreDefinitions{

  


  
     //grapple hook C1

   def invariantR1(speed : Float, stoppointup: Int) : Invariant ={
	var inv : List[Invariant] = Nil;
	  
	for (i <- 0 to 100) {
	  inv ::= (IMPLIES(TimeStamp (i), 
	      OccupySegment3D(300,200+(i*speed).toInt,0,300,220+(i*speed).toInt,0,3)))	
	}
	for (i <- 0 to 100) {
	  if (i < stoppointup) {
		  inv ::= (IMPLIES(TimeStamp (i+101), 
				  OccupySegment3D(300,200+(100*speed).toInt-(i*speed).toInt,0,300,220+(100*speed).toInt-(i*speed).toInt,0,3)))	
	  } else {
		  inv ::= (IMPLIES(TimeStamp (i+101), 
				  OccupySegment3D(300,200+(100*speed).toInt-(stoppointup*speed).toInt,0,300,220+(100*speed).toInt-(stoppointup*speed).toInt,0,3)))	
	  }
	}
	return (BIGAND(inv));
   }
   
    def invariantR2(speed : Float, stoppointup: Int) : Invariant ={
	var inv : List[Invariant] = Nil;
	  
	for (i <- 0 to 100) {
	  inv ::= (IMPLIES(TimeStamp (i), 
	      OccupySegment3D(320,200+(i*speed).toInt,0,320,220+(i*speed).toInt,0,3)))	
	}
	for (i <- 0 to 100) {
	  	  if (i < stoppointup) {
	  		  inv ::= (IMPLIES(TimeStamp (i+101), 
	  				  OccupySegment3D(320,200+(100*speed).toInt-(i*speed).toInt,0,320,220+(100*speed).toInt-(i*speed).toInt,0,3)))
	  	  } else {
	  		  inv ::= (IMPLIES(TimeStamp (i+101), 
	  				  OccupySegment3D(320,200+(100*speed).toInt-(stoppointup*speed).toInt,0,320,220+(100*speed).toInt-(stoppointup*speed).toInt,0,3)))	  	    
	  	  }
	
	}
	return (BIGAND(inv));
   }
    
   def invariantR3(speed : Float, stoppointup: Int) : Invariant ={
	var inv : List[Invariant] = Nil;
	  
	for (i <- 0 to 100) {
	  inv ::= (IMPLIES(TimeStamp (i), 
	      OccupySegment3D(300,200+(i*speed).toInt,0,320,200+(i*speed).toInt,0,3)))	
	}
	for (i <- 0 to 100) {
	  	  if (i < stoppointup) {
	  		  inv ::= (IMPLIES(TimeStamp (i+101), 
	  				  OccupySegment3D(300,200+(100*speed).toInt-(i*speed).toInt,0,320,200+(100*speed).toInt-(i*speed).toInt,0,3)))  	    
	  	  } else {
	  		  inv ::= (IMPLIES(TimeStamp (i+101), 
	  				  OccupySegment3D(300,200+(100*speed).toInt-(stoppointup*speed).toInt,0,320,200+(100*speed).toInt-(stoppointup*speed).toInt,0,3)))  	  	    
	  	  }

	
	}
	return (BIGAND(inv));
   }
   
    def invariantR4(speed : Float, stoppointup: Int) : Invariant ={
	var inv : List[Invariant] = Nil;
	  
	for (i <- 0 to 100) {
	  inv ::= (IMPLIES(TimeStamp (i), 
	      OccupySegment3D(310,100,0,310,200+(i*speed).toInt,0,3)))	
	}
	for (i <- 0 to 100) {
	  	  if (i < stoppointup) {  	    
	  		  inv ::= (IMPLIES(TimeStamp (i+101), 
	  				  OccupySegment3D(310,100,0,310,200+(100*speed).toInt-(i*speed).toInt,0,3)))	
	  	  } else {
	  		  inv ::= (IMPLIES(TimeStamp (i+101), 
	  				  OccupySegment3D(310,100,0,310,200+(100*speed).toInt-(stoppointup*speed).toInt,0,3)))		  	    
	  	  }

	  	    
	}
	return (BIGAND(inv));
   }
    
   def invariantWP(speed : Float, stoppointup: Int) : Invariant ={
	var inv : List[Invariant] = Nil;
	  
	for (i <- 0 to 100) {
	  inv ::= (IMPLIES(TimeStamp (i), 
	      //OccupyBox(100,100,20,20)))
	      Occupy3DBox(305,205 + (100*speed).toInt,0,315,215 + (100 * speed).toInt,3)))	
	}
	for (i <- 0 to 100) {
	  	  if (i < stoppointup) { 
	  		  inv ::= (IMPLIES(TimeStamp (i+101), 
	  				  Occupy3DBox(305,200 + 5 + (100*speed).toInt-(i*speed).toInt,0,315,200 + 15 + (100 *speed).toInt-(i*speed).toInt,3)))	
	  	  } else {
	  		  inv ::= (IMPLIES(TimeStamp (i+101), 
	  				  Occupy3DBox(305,200 + 5 + (100*speed).toInt-(stoppointup*speed).toInt,0,315,200 + 15 + (100 *speed).toInt-(stoppointup*speed).toInt,3)))		  	    
	  	  }

	}
	return (BIGAND(inv));
   }
  
   // BehT for the Hook

   
  
  def behtcomp1def (speed:Float,stpu : Int) =
		  SimpleSpatioTemporalBehavioralType ("Hook", BIGAND(invariantR1(speed,stpu)::invariantR2(speed,stpu)::invariantR3(speed,stpu)::invariantR4(speed,stpu)::Nil), "H"::Nil , Map()("H" -> invariantR2(speed,stpu))) 
  
  
  def main(args: Array[String]) {
  	println("x")
  
  	
  	//Visualization3D.setInvariant(BIGAND(Occupy3DBox(100,100,100,200,200,200)::Occupy3DBox(100,100,200,200,200,300)::Occupy3DBox(100,200,200,200,300,300)::Occupy3DBox(200,200,200,250,300,300)::Nil ));
  	
  	Visualization3D.setInvariant(BIGAND(
	      simplifyInvariant((getSubInvariantForTime(99,invariantWP(1,200))))::
	      this.OccupySegment2OccupyBoxes3D(simplifyInvariant((getSubInvariantForTime(99,invariantR1(1,200)))))::
	      this.OccupySegment2OccupyBoxes3D(simplifyInvariant((getSubInvariantForTime(99,invariantR2(1,200)))))::
	      this.OccupySegment2OccupyBoxes3D(simplifyInvariant((getSubInvariantForTime(99,invariantR3(1,200)))))::
	      this.OccupySegment2OccupyBoxes3D(simplifyInvariant((getSubInvariantForTime(99,invariantR4(1,200)))))::Nil));
  	Visualization3D.doGraphics();
  	    var xval = 5.0d;
    var dir : Boolean = true;
    //doGraphics();
    for (i <- 0 to 10000) {
    Thread.sleep(10);
    
    Visualization3D.viewUpdate(xval,0.1,0.1);
    if (xval >= 12d || xval <= 1d) dir = ! dir;
    if (dir)
    	xval += 0.01d;
    else
    	xval -= 0.01d
    }
    /*
	val vis2d = new Visualization2D();
	vis2d.startup(null);

	for (i <- 0 to 200) {
	  Thread.sleep(100)
	  vis2d.setInvariant(BIGAND(
	      simplifyInvariant(this.unfoldInvariant(getSubInvariantForTime(i,invariantR1(2.5f,15))))::
	      simplifyInvariant(this.unfoldInvariant(getSubInvariantForTime(i,invariantR2(2.5f,15))))::
	      simplifyInvariant(this.unfoldInvariant(getSubInvariantForTime(i,invariantR3(2.5f,15))))::
	      simplifyInvariant(this.unfoldInvariant(getSubInvariantForTime(i,invariantR4(2.5f,15))))::

	      simplifyInvariant(getSubInvariantForTime(i,invariantWP(2.5f,15)))::

	      Nil
	  ))
	  vis2d.panel.repaint
	}
	*/
  }
}