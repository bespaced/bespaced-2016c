
/*
* Jan Olaf Blech 
* RMIT University
* 2013
*/
package BeSpaceD3D

import java.awt.Color;
import com.sun.j3d.utils.geometry._;
import com.sun.j3d.utils.geometry.GeometryInfo;
import com.sun.j3d.utils.geometry.NormalGenerator;
import com.sun.j3d.utils.universe.SimpleUniverse;
import com.sun.j3d.utils.geometry.Box;
import javax.media.j3d._;
import javax.vecmath._;


object Visualization3DTest extends App {

 var box :Box = new Box();
 var universe : SimpleUniverse = new SimpleUniverse();
 var group : BranchGroup = new BranchGroup();    

  
  def doGraphics() {
  		
		//group.addChild(new Sphere(0.5f));

		universe = new SimpleUniverse();
		
		val invariantvis1 : QuadArray = new QuadArray(360,1);
		
		
		//invariantvis1.
		/*
		for (i <- 0 to 29) {
		  
			invariantvis1.setCoordinate(i*4, new Point3f(-0.5f,0.0f,1.0f - (0.1f * (i.floatValue()))));
			invariantvis1.setCoordinate(i*4+1,new Point3f(-0.5f,-0.1f,1.0f - (0.1f * (i.floatValue()))));
			invariantvis1.setCoordinate(i*4+2,new Point3f(-0.4f,-0.1f,1.0f - (0.1f * (i.floatValue()))));
			invariantvis1.setCoordinate(i*4+3,new Point3f(-0.4f,0.0f,1.0f - (0.1f * (i.floatValue()))));
		}

		
		for (i <- 0 to 29) {
	
			invariantvis1.setCoordinate(120+i*4, new Point3f(-0.3f,0.3f+(0.02f * (i.floatValue())),1.0f - (0.1f * (i.floatValue()))));
			invariantvis1.setCoordinate(120+i*4+1,new Point3f(-0.3f,0.2f+(0.02f * (i.floatValue())),1.0f - (0.1f * (i.floatValue()))));
			invariantvis1.setCoordinate(120+i*4+2,new Point3f(-0.15f,0.2f+(0.02f * (i.floatValue())),1.0f - (0.1f * (i.floatValue()))));
			invariantvis1.setCoordinate(120+i*4+3,new Point3f(-0.15f,0.3f+(0.02f * (i.floatValue())),1.0f - (0.1f * (i.floatValue()))));
		}
		
		for (i <- 0 to 29) {
	
			invariantvis1.setCoordinate(240+i*4, new Point3f(0.3f-(0.03f * (i.floatValue())),0.3f+(0.01f * (i.floatValue())),1.0f - (0.1f * (i.floatValue()))));
			invariantvis1.setCoordinate(240+i*4+1,new Point3f(0.3f-(0.03f * (i.floatValue())),0.2f+(0.01f * (i.floatValue())),1.0f - (0.1f * (i.floatValue()))));
			invariantvis1.setCoordinate(240+i*4+2,new Point3f(0.4f-(0.03f * (i.floatValue())),0.2f+(0.01f * (i.floatValue())),1.0f - (0.1f * (i.floatValue()))));
			invariantvis1.setCoordinate(240+i*4+3,new Point3f(0.4f-(0.03f * (i.floatValue())),0.3f+(0.01f * (i.floatValue())),1.0f - (0.1f * (i.floatValue()))));

		}*/
	box = new Box(1.0f,1.0f,1.0f, new Appearance());
		//val geometryInfo :GeometryInfo = new GeometryInfo(box.getShape(1));
		val geometryInfo :GeometryInfo = new GeometryInfo(invariantvis1);
		//geometryInfo.
		val ng : NormalGenerator = new NormalGenerator();
		ng.generateNormals(geometryInfo);

		val result : GeometryArray = geometryInfo.getGeometryArray();
		val appearance  : Appearance = new Appearance();
		val color : Color3f = new Color3f(Color.yellow);
		val black : Color3f = new Color3f(0.0f, 0.0f, 0.0f);
		val white : Color3f = new Color3f(1.0f, 1.0f, 1.0f);
		val texture : Texture = new Texture2D();
		val texAttr : TextureAttributes = new TextureAttributes();
		texAttr.setTextureMode(TextureAttributes.MODULATE);
		texture.setBoundaryModeS(Texture.WRAP);
		texture.setBoundaryModeT(Texture.WRAP);
		texture.setBoundaryColor(new Color4f(0.0f, 1.0f, 0.0f, 0.5f));
		val mat : Material = new Material(color, black, color, white, 70f);
		appearance.setTextureAttributes(texAttr);
		appearance.setMaterial(mat);
		appearance.setTexture(texture);
		val ta : TransparencyAttributes = new TransparencyAttributes();
		ta.setTransparencyMode(1);
		ta.setTransparency(0.5f);
		
		appearance.setTransparencyAttributes(ta);
		val shape : Shape3D = new Shape3D(result, appearance);
		group  = new BranchGroup();
		//group.addChild(shape);
		
		val tg : TransformGroup = new TransformGroup();

        val transform :Transform3D = new Transform3D();

        val vector : Vector3f = new Vector3f( 0f, .5f, .0f);

      transform.setTranslation(vector);

      tg.setTransform(transform);
		box = new Box (.1f,.1f,.1f,appearance)

      tg.addChild(box);
		// box = new Box (result,appearance)

		group.addChild(tg)
		
				val tg2 : TransformGroup = new TransformGroup();

        val transform2 :Transform3D = new Transform3D();

        val vector2 : Vector3f = new Vector3f( .5f, .0f, .0f);

      transform2.setTranslation(vector2);

      tg2.setTransform(transform2);
		val box2 = new Box (.1f,.1f,.1f,appearance)

      tg2.addChild(box2);
		// box = new Box (result,appearance)

		group.addChild(tg2)
	
		val viewTranslation : Vector3f = new Vector3f();
		viewTranslation.z = 3f;
		viewTranslation.x = 0f;
		viewTranslation.y = 0f;
		val viewTransform : Transform3D  = new Transform3D();
		viewTransform.setTranslation(viewTranslation);
		val rotation : Transform3D = new Transform3D();
		rotation.rotX(-Math.PI / 3.1d / 2.5d);
		rotation.rotY(-Math.PI / 3.1d / 2.4d);
		rotation.rotZ(-Math.PI/ 3.1d / 2.4d)
		
		rotation.mul(viewTransform);
		universe.getViewingPlatform().getViewPlatformTransform().setTransform(
				rotation);
		universe.getViewingPlatform().getViewPlatformTransform().getTransform(
				viewTransform);
		
		
		val bounds : BoundingSphere = new BoundingSphere(new Point3d(0.0, 0.0, 2.0),1000.0);
		val light1Color : Color3f  = new Color3f(.8f, .8f, .8f);
		val light1Direction : Vector3f  = new Vector3f(-10.0f, -7.0f, -4.0f);
		val light1 : DirectionalLight  = new DirectionalLight(light1Color, light1Direction);
		light1.setInfluencingBounds(bounds);
		group.addChild(light1);
		val ambientColor : Color3f  = new Color3f(1f, 1f, .1f);
		val ambientLightNode : AmbientLight  = new AmbientLight(ambientColor);
		ambientLightNode.setInfluencingBounds(bounds);
		group.addChild(ambientLightNode);
		
		universe.addBranchGraph(group);
		//universe.getCanvas().
  }
  
  def viewUpdate(xval: Double) {
    //int x = box.getXdimension();
    println("x")
    /*
    universe = new SimpleUniverse()
    //group.removeChild(box)
    box = new Box (.5f,.5f,.5f,new Appearance())
    group = new BranchGroup()
    
    val viewTranslation : Vector3f = new Vector3f();
		viewTranslation.z = 3;
		viewTranslation.x = 0f;
		viewTranslation.y = 0f;
		val viewTransform : Transform3D  = new Transform3D();
		viewTransform.setTranslation(viewTranslation);
		val rotation : Transform3D = new Transform3D();
		rotation.rotX(-Math.PI / 3.1d);
		rotation.rotY(-Math.PI / 3.1d);
		rotation.rotZ(-Math.PI/ 3.1d)
		
		rotation.mul(viewTransform);
		universe.getViewingPlatform().getViewPlatformTransform().setTransform(
				rotation);
		universe.getViewingPlatform().getViewPlatformTransform().getTransform(
				viewTransform);
		
		
		val bounds : BoundingSphere = new BoundingSphere(new Point3d(0.0, 0.0, 2.0),1000.0);
		val light1Color : Color3f  = new Color3f(.8f, .8f, .8f);
		val light1Direction : Vector3f  = new Vector3f(-10.0f, -7.0f, -4.0f);
		val light1 : DirectionalLight  = new DirectionalLight(light1Color, light1Direction);
		light1.setInfluencingBounds(bounds);
		group.addChild(light1);
		val ambientColor : Color3f  = new Color3f(1f, 1f, .1f);
		val ambientLightNode : AmbientLight  = new AmbientLight(ambientColor);
		ambientLightNode.setInfluencingBounds(bounds);
		group.addChild(ambientLightNode);
    group.addChild(box)
    universe.addBranchGraph(group)
*/
    val viewTranslation : Vector3f = new Vector3f();
		viewTranslation.z = 3;
		viewTranslation.x = 0f;
		viewTranslation.y = 0f;
		val viewTransform : Transform3D  = new Transform3D();
		viewTransform.setTranslation(viewTranslation);
		val rotation : Transform3D = new Transform3D();
		rotation.rotX(-Math.PI / xval);
		//rotation.rotY(-Math.PI / 12d);
		//rotation.rotZ(-Math.PI/ 12d)
		
		rotation.mul(viewTransform);
		universe.getViewingPlatform().getViewPlatformTransform().setTransform(
				rotation);
		universe.getViewingPlatform().getViewPlatformTransform().getTransform(
				viewTransform);
    
    
  }
  
  override def main(args: Array[String]) {
    var xval = 5.0d;
    var dir : Boolean = true;
    doGraphics();
    for (i <- 0 to 10000) {
    Thread.sleep(10);
    
    viewUpdate(xval);
    if (xval >= 12d || xval <= 1d) dir = ! dir;
    if (dir)
    	xval += 0.01d;
    else
    	xval -= 0.01d
    }
 

		
  }
}