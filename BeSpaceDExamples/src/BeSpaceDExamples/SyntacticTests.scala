package BeSpaceDExamples

import BeSpaceDCore._;

/**
 * @author janblech
 */
object SyntacticTests {
  def example1 = IMPLIES(AND(TimeInterval(10,20),Owner("test")),OccupyBox(100,200,400,500))
}