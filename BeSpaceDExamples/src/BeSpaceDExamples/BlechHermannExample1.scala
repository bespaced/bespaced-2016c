/* 
 * Created 11/09/2013
 * may need runtime setting -Xss515m or similar,  */

package BeSpaceDExamples
import BeSpaceDCore._;
object BlechHerrmannExample1 extends CoreDefinitions{

  val robotinvariantAbs1 = BIGAND ((OccupyBox(100,140,1100,160))::Nil);
  val robotinvariantAbs2 = BIGAND ((OccupyBox(100,140,1100,160))::(BIGOR((ComponentState("FAST"))::(ComponentState("SLOW"))::(ComponentState("STANDING"))::Nil))::Nil);
  

  


 
  def robotinvariantAbs1wt() : Invariant ={
	var rai : List[Invariant] = Nil;
	for (i <- 0 to 240) {
	  
	  rai ::= (IMPLIES(TimeStamp (i),(OccupyBox(100,140,1100,160))));
	}
	return (BIGAND (rai));    
  }
  
  def humanapproachingLeft() : Invariant ={
	var hali : List[Invariant] = Nil;
	for (i <- 0 to 240) {
	  hali ::= (IMPLIES(TimeStamp (i), (OccupyBox(0+(i/2),0,0+(i/2),300))));
	}
	return (BIGAND (hali));    
  }
 
  
  /*
   * normal behavior for a robot that moves from left to right
   */
  def robotinvariantPhysicsGenNormalBehavior() : Invariant ={
    val timeraster = 5.0; // in ms 
	var rai : List[Invariant] = Nil;
  	var speed : Double = 0;
  	var d : Double = 0;
  	var t : Int = 0;
  	rai ::= (IMPLIES(TimeStamp (t),(OccupyBox((99 + d).toInt,140,(121 + d).toInt,160))));

	while(d < 980) {
	  if (d <= 870) {
		  if (speed < 10) {
			  speed += (5.0 / 1000.0) / 10.0 * timeraster; //acceleration / s -> ms / m -> m/10
		  }
	  } else {
		  if (speed > 1) {
			  speed -= (5.0 / 1000.0) * timeraster;
		  }
	  }
	  t+=1;
	  d += (speed / 1000.0) * 10.0 * timeraster ;
	  rai ::= (IMPLIES(TimeStamp (t),(OccupyBox((99 + d).toInt,140,(121 + d).toInt,160))));

	}
	return (BIGAND (rai));    
  }
  
    def robotinvariantPhysicsGenNormalBehaviorCoarse() : Invariant ={
    val timeraster = 5.0; // in ms 
	var rai : List[Invariant] = Nil;
  	var speed : Double = 0;
  	var d : Double = 0;
  	var t : Int = 0;
  	rai ::= (IMPLIES(TimeStamp (t),(OccupyBox((98 + d).toInt,139,(122 + d).toInt,161))));

	while(d < 980) {
	  if (d <= 870) {
		  if (speed < 10) {
			  speed += (5.0 / 1000.0) / 10.0 * timeraster; //acceleration / s -> ms / m -> m/10
		  }
	  } else {
		  if (speed > 1) {
			  speed -= (5.0 / 1000.0) * timeraster;
		  }
	  }
	  t+=1;
	  d += (speed / 1000.0) * 10.0 * timeraster ;
	  rai ::= (IMPLIES(TimeStamp (t),(OccupyBox((98 + d).toInt,139,(122 + d).toInt,161))));

	}
	return (BIGAND (rai));    
  }

  def main(args: Array[String]) {

		// Run all the tests once.
		testTopologicalGeometric()
		testPhysics()
		testComparisonArctis()
		testNewComparisonArctis()
		testNextNewComparisonArctis()
	}



	// ---------------------------------------------------------------------- Topological / Geometric

	def testTopologicalGeometric()
	{
		println("Topological Invariants:");
		println("Geometric Invariants:");
		for (i <- 0 to 240) {
			println(prettyPrintInvariant(this.getSubInvariantForTime(i, simplifyInvariant(humanapproachingLeft))) + "   " +
				prettyPrintInvariant(this.getSubInvariantForTime(i, simplifyInvariant(robotinvariantAbs1wt))));

		}
	}



	// ---------------------------------------------------------------------- Physics

	def testPhysics()
	{
		//println (this.robotinvariantPhysicsGenNormalBehavior);
		println("Normal Physics behavior");
		println(prettyPrintInvariant(this.getSubInvariantForTime(10, this.robotinvariantPhysicsGenNormalBehavior)));

		var t: Int = 0;
		while (!(this.getSubInvariantForTime(t, this.robotinvariantPhysicsGenNormalBehavior)).equals(TRUE())) {
			println(t * 5 + "ms : " + this.getSubInvariantForTime(t, this.robotinvariantPhysicsGenNormalBehavior));
			t += 1;

		}
	}



	// ---------------------------------------------------------------------- Comparison Arctis

	def testComparisonArctis() {
		println("Comparison Arctis <-> BeSpaced");
		for (i <- 1110 to 1187) {
			println(prettyPrintInvariant(this.getSubInvariantForTime(i, BIGAND(arcticexamplegeneration2robotinv2))) + "   " +
				prettyPrintInvariant(this.getSubInvariantForTime(i, robotinvariantPhysicsGenNormalBehavior)) + " " +
				this.inclusionBox(
					getSubInvariantForTime(i, robotinvariantPhysicsGenNormalBehavior),
					getSubInvariantForTime(i, BIGAND(arcticexamplegeneration2robotinv2))).toString + " " +
				this.inclusionBox(
					getSubInvariantForTime(i, BIGAND(arcticexamplegeneration2robotinv2)),
					getSubInvariantForTime(i, robotinvariantPhysicsGenNormalBehavior)).toString);
			;
		}
	}



	// ---------------------------------------------------------------------- New Comparison Arctis

	def testNewComparisonArctis()
	{
		println("New Comparison Arctis <-> BeSpaced");
		for (i <- 0 to 110) {
			println(prettyPrintInvariant(this.getSubInvariantForTime(i, BIGAND(newrobotrunundisturbed))) + "   " +
				prettyPrintInvariant(this.getSubInvariantForTime(i, robotinvariantPhysicsGenNormalBehaviorCoarse)) + " " +
				this.inclusionBox(
					getSubInvariantForTime(i, robotinvariantPhysicsGenNormalBehaviorCoarse),
					getSubInvariantForTime(i, BIGAND(newrobotrunundisturbed))).toString + " " +
				this.inclusionBox(
					getSubInvariantForTime(i, BIGAND(newrobotrunundisturbed)),
					getSubInvariantForTime(i, robotinvariantPhysicsGenNormalBehaviorCoarse)).toString);
			;
		}
	}



	// ---------------------------------------------------------------------- Next Version of New Comparison Arctis

	def testNextNewComparisonArctis()
	{
		println("Next Version of New Comparison Arctis <-> BeSpaced");
		for (i <- 0 to 100) {
			println(prettyPrintInvariant(this.getSubInvariantForTime(i, BIGAND(newrobotrunundisturbed2))) + "   " +
				prettyPrintInvariant(this.getSubInvariantForTime(i, robotinvariantPhysicsGenNormalBehavior)) + " " +
				this.inclusionBox(
					getSubInvariantForTime(i, robotinvariantPhysicsGenNormalBehavior),
					getSubInvariantForTime(i, BIGAND(newrobotrunundisturbed2))).toString + " " +
				this.inclusionBox(
					getSubInvariantForTime(i, BIGAND(newrobotrunundisturbed2)),
					getSubInvariantForTime(i, robotinvariantPhysicsGenNormalBehavior)).toString);
			;
		}
	}
       
    //println(this.getSubInvariantForTime(1,this.robotinvariantPhysicsGenNormalBehavior));


      //  println (this.collisionTestsAlt1( 
     //       this.simplifyInvariant(this.unfoldInvariant(getSubInvariantForTime(i,invari()))):: this.simplifyInvariant(this.unfoldInvariant(getSubInvariantForTime(i,invariant1bgeneration())))::Nil, 
    //        this.simplifyInvariant(this.unfoldInvariant(getSubInvariantForTime(i,invariant2bgeneration()))):: this.simplifyInvariant(this.unfoldInvariant(getSubInvariantForTime(i,invariant2ageneration())))::Nil));
        
    //    println (this.collisionTestsAlt1( 
    //        this.simplifyInvariant(this.unfoldInvariant(getSubInvariantForTime(i,invariant1aBIGANDgeneration()))):: this.simplifyInvariant(this.unfoldInvariant(getSubInvariantForTime(i,invariant1bBIGANDgeneration())))::Nil, 
    //        this.simplifyInvariant(this.unfoldInvariant(getSubInvariantForTime(i,invariant2bBIGANDgeneration()))):: this.simplifyInvariant(this.unfoldInvariant(getSubInvariantForTime(i,invariant2aBIGANDgeneration())))::Nil));

    	//println (this.collisionTests( getSubInvariantForTime(i,invariant2bgeneration()) :: Nil, getSubInvariantForTime(i,invariant2bgeneration())::Nil));
    

  
  //excerpt
    val arcticexamplegeneration1 : List[Invariant] = 
      (IMPLIES (TimeStamp ( 0),OccupyBox ( 109 , 140 , 131 , 160))) ::
		(IMPLIES (TimeStamp ( 1),OccupyBox ( 109 , 140 , 131 , 160))) ::
		Nil;
      
    //excerpt
     val arcticexamplegeneration2robotinv : List[Invariant] = 
      (IMPLIES (TimeStamp ( 0),OccupyBox ( 109 , 140 , 131 , 160))) ::
		(IMPLIES (TimeStamp ( 1),OccupyBox ( 109 , 140 , 131 , 160))) ::
		Nil;
     
    // another excerpt
     val arcticexamplegeneration2robotinv2 : List[Invariant] = 
        (IMPLIES (TimeStamp ( 1110),OccupyBox ( 353 , 140 , 377 , 160))) ::
		(IMPLIES (TimeStamp ( 1111),OccupyBox ( 354 , 140 , 378 , 160))) ::
		(IMPLIES (TimeStamp ( 1112),OccupyBox ( 354 , 140 , 378 , 160))) ::
		(IMPLIES (TimeStamp ( 1113),OccupyBox ( 355 , 140 , 379 , 160))) ::
		(IMPLIES (TimeStamp ( 1114),OccupyBox ( 355 , 140 , 379 , 160))) ::
		(IMPLIES (TimeStamp ( 1115),OccupyBox ( 356 , 140 , 380 , 160))) ::
		(IMPLIES (TimeStamp ( 1116),OccupyBox ( 356 , 140 , 380 , 160))) ::
		(IMPLIES (TimeStamp ( 1117),OccupyBox ( 357 , 140 , 381 , 160))) ::
		(IMPLIES (TimeStamp ( 1118),OccupyBox ( 357 , 140 , 381 , 160))) ::
		(IMPLIES (TimeStamp ( 1119),OccupyBox ( 358 , 140 , 382 , 160))) ::
		(IMPLIES (TimeStamp ( 1120),OccupyBox ( 358 , 140 , 382 , 160))) ::
		(IMPLIES (TimeStamp ( 1121),OccupyBox ( 359 , 140 , 383 , 160))) ::
		(IMPLIES (TimeStamp ( 1122),OccupyBox ( 359 , 140 , 383 , 160))) ::
		(IMPLIES (TimeStamp ( 1123),OccupyBox ( 360 , 140 , 384 , 160))) ::
		(IMPLIES (TimeStamp ( 1124),OccupyBox ( 360 , 140 , 384 , 160))) ::
		(IMPLIES (TimeStamp ( 1125),OccupyBox ( 361 , 140 , 385 , 160))) ::
		(IMPLIES (TimeStamp ( 1126),OccupyBox ( 361 , 140 , 385 , 160))) ::
		(IMPLIES (TimeStamp ( 1127),OccupyBox ( 362 , 140 , 386 , 160))) ::
		(IMPLIES (TimeStamp ( 1128),OccupyBox ( 362 , 140 , 386 , 160))) ::
		(IMPLIES (TimeStamp ( 1129),OccupyBox ( 363 , 140 , 387 , 160))) ::
		(IMPLIES (TimeStamp ( 1130),OccupyBox ( 363 , 140 , 387 , 160))) ::
		(IMPLIES (TimeStamp ( 1131),OccupyBox ( 364 , 140 , 388 , 160))) ::
		(IMPLIES (TimeStamp ( 1132),OccupyBox ( 364 , 140 , 388 , 160))) ::
		(IMPLIES (TimeStamp ( 1133),OccupyBox ( 365 , 140 , 389 , 160))) ::
		(IMPLIES (TimeStamp ( 1134),OccupyBox ( 365 , 140 , 389 , 160))) ::
		(IMPLIES (TimeStamp ( 1135),OccupyBox ( 366 , 140 , 390 , 160))) ::
		(IMPLIES (TimeStamp ( 1136),OccupyBox ( 366 , 140 , 390 , 160))) ::
		(IMPLIES (TimeStamp ( 1137),OccupyBox ( 367 , 140 , 391 , 160))) ::
		(IMPLIES (TimeStamp ( 1138),OccupyBox ( 367 , 140 , 391 , 160))) ::
		(IMPLIES (TimeStamp ( 1139),OccupyBox ( 368 , 140 , 392 , 160))) ::
		(IMPLIES (TimeStamp ( 1140),OccupyBox ( 368 , 140 , 392 , 160))) ::
		(IMPLIES (TimeStamp ( 1141),OccupyBox ( 369 , 140 , 393 , 160))) ::
		(IMPLIES (TimeStamp ( 1142),OccupyBox ( 369 , 140 , 393 , 160))) ::
		(IMPLIES (TimeStamp ( 1143),OccupyBox ( 370 , 140 , 394 , 160))) ::
		(IMPLIES (TimeStamp ( 1144),OccupyBox ( 370 , 140 , 394 , 160))) ::
		(IMPLIES (TimeStamp ( 1145),OccupyBox ( 371 , 140 , 395 , 160))) ::
		(IMPLIES (TimeStamp ( 1146),OccupyBox ( 371 , 140 , 395 , 160))) ::
		(IMPLIES (TimeStamp ( 1147),OccupyBox ( 372 , 140 , 396 , 160))) ::
		(IMPLIES (TimeStamp ( 1148),OccupyBox ( 372 , 140 , 396 , 160))) ::
		(IMPLIES (TimeStamp ( 1149),OccupyBox ( 373 , 140 , 397 , 160))) ::
		(IMPLIES (TimeStamp ( 1150),OccupyBox ( 373 , 140 , 397 , 160))) ::
		(IMPLIES (TimeStamp ( 1151),OccupyBox ( 374 , 140 , 398 , 160))) ::
		(IMPLIES (TimeStamp ( 1152),OccupyBox ( 374 , 140 , 398 , 160))) ::
		(IMPLIES (TimeStamp ( 1153),OccupyBox ( 375 , 140 , 399 , 160))) ::
		(IMPLIES (TimeStamp ( 1154),OccupyBox ( 375 , 140 , 399 , 160))) ::
		(IMPLIES (TimeStamp ( 1155),OccupyBox ( 376 , 140 , 400 , 160))) ::
		(IMPLIES (TimeStamp ( 1156),OccupyBox ( 376 , 140 , 400 , 160))) ::
		(IMPLIES (TimeStamp ( 1157),OccupyBox ( 377 , 140 , 401 , 160))) ::
		(IMPLIES (TimeStamp ( 1158),OccupyBox ( 377 , 140 , 401 , 160))) ::
		(IMPLIES (TimeStamp ( 1159),OccupyBox ( 378 , 140 , 402 , 160))) ::
		(IMPLIES (TimeStamp ( 1160),OccupyBox ( 378 , 140 , 402 , 160))) ::
		(IMPLIES (TimeStamp ( 1161),OccupyBox ( 379 , 140 , 403 , 160))) ::
		(IMPLIES (TimeStamp ( 1162),OccupyBox ( 379 , 140 , 403 , 160))) ::
		(IMPLIES (TimeStamp ( 1163),OccupyBox ( 380 , 140 , 404 , 160))) ::
		(IMPLIES (TimeStamp ( 1164),OccupyBox ( 380 , 140 , 404 , 160))) ::
		(IMPLIES (TimeStamp ( 1165),OccupyBox ( 381 , 140 , 405 , 160))) ::
		(IMPLIES (TimeStamp ( 1166),OccupyBox ( 381 , 140 , 405 , 160))) ::
		(IMPLIES (TimeStamp ( 1167),OccupyBox ( 382 , 140 , 406 , 160))) ::
		(IMPLIES (TimeStamp ( 1168),OccupyBox ( 382 , 140 , 406 , 160))) ::
		(IMPLIES (TimeStamp ( 1169),OccupyBox ( 383 , 140 , 407 , 160))) ::
		(IMPLIES (TimeStamp ( 1170),OccupyBox ( 383 , 140 , 407 , 160))) ::
		(IMPLIES (TimeStamp ( 1171),OccupyBox ( 383 , 140 , 407 , 160))) ::
		(IMPLIES (TimeStamp ( 1172),OccupyBox ( 384 , 140 , 408 , 160))) ::
		(IMPLIES (TimeStamp ( 1173),OccupyBox ( 384 , 140 , 408 , 160))) ::
		(IMPLIES (TimeStamp ( 1174),OccupyBox ( 385 , 140 , 409 , 160))) ::
		(IMPLIES (TimeStamp ( 1175),OccupyBox ( 385 , 140 , 409 , 160))) ::
		(IMPLIES (TimeStamp ( 1176),OccupyBox ( 386 , 140 , 410 , 160))) ::
		(IMPLIES (TimeStamp ( 1177),OccupyBox ( 386 , 140 , 410 , 160))) ::
		(IMPLIES (TimeStamp ( 1178),OccupyBox ( 387 , 140 , 411 , 160))) ::
		(IMPLIES (TimeStamp ( 1179),OccupyBox ( 387 , 140 , 411 , 160))) ::
		(IMPLIES (TimeStamp ( 1180),OccupyBox ( 387 , 140 , 411 , 160))) ::
		(IMPLIES (TimeStamp ( 1181),OccupyBox ( 388 , 140 , 412 , 160))) ::
		(IMPLIES (TimeStamp ( 1182),OccupyBox ( 388 , 140 , 412 , 160))) ::
		(IMPLIES (TimeStamp ( 1183),OccupyBox ( 389 , 140 , 413 , 160))) ::
		(IMPLIES (TimeStamp ( 1184),OccupyBox ( 389 , 140 , 413 , 160))) ::
		(IMPLIES (TimeStamp ( 1185),OccupyBox ( 390 , 140 , 414 , 160))) ::
		(IMPLIES (TimeStamp ( 1186),OccupyBox ( 390 , 140 , 414 , 160))) ::
		(IMPLIES (TimeStamp ( 1187),OccupyBox ( 391 , 140 , 415 , 160))) ::
		Nil;
		     
     //excerpt
     val arcticexamplegeneration2humaninv : List[Invariant] = 
      (IMPLIES (TimeStamp ( 0),OccupyBox ( 109 , 140 , 131 , 160))) ::
		(IMPLIES (TimeStamp ( 1),OccupyBox ( 109 , 140 , 131 , 160))) ::
		Nil;
      
  	val newrobotrunundisturbed : List[Invariant] = 
  	  (IMPLIES (TimeStamp ( 0),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 1),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 2),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 3),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 4),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 5),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 6),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 7),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 8),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 9),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 10),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 11),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 12),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 13),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 14),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 15),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 16),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 17),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 18),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 19),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 20),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 21),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 22),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 23),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 24),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 25),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 26),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 27),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 28),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 29),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 30),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 31),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 32),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 33),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 34),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 35),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 36),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 37),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 38),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 39),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 40),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 41),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 42),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 43),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 44),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 45),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 46),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 47),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 48),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 49),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 50),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 51),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 52),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 53),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 54),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 55),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 56),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 57),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 58),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 59),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 60),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 61),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 62),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 63),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 64),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 65),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 66),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 67),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 68),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 69),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 70),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 71),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 72),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 73),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 74),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 75),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 76),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 77),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 78),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 79),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 80),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 81),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 82),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 83),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 84),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 85),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 86),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 87),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 88),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 89),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 90),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 91),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 92),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 93),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 94),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 95),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 96),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 97),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 98),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 99),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 100),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 101),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 102),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 103),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 104),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 105),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 106),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 107),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 108),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 109),OccupyBox ( 98 , 140 , 122 , 160))) ::
		(IMPLIES (TimeStamp ( 110),OccupyBox ( 98 , 140 , 122 , 160))) ::
		Nil;
  	
    	val newrobotrunundisturbed2 : List[Invariant] = //without safety margins
	
	  	(IMPLIES (TimeStamp ( 0),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 1),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 2),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 3),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 4),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 5),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 6),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 7),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 8),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 9),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 10),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 11),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 12),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 13),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 14),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 15),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 16),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 17),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 18),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 19),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 20),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 21),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 22),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 23),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 24),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 25),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 26),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 27),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 28),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 29),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 30),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 31),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 32),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 33),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 34),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 35),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 36),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 37),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 38),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 39),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 40),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 41),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 42),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 43),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 44),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 45),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 46),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 47),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 48),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 49),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 50),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 51),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 52),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 53),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 54),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 55),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 56),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 57),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 58),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 59),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 60),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 61),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 62),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 63),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 64),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 65),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 66),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 67),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 68),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 69),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 70),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 71),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 72),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 73),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 74),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 75),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 76),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 77),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 78),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 79),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 80),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 81),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 82),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 83),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 84),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 85),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 86),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 87),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 88),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 89),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 90),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 91),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 92),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 93),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 94),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 95),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 96),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 97),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 98),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 99),OccupyBox ( 100 , 140 , 120 , 160))) ::
		(IMPLIES (TimeStamp ( 100),OccupyBox ( 100 , 140 , 120 , 160))) ::
		Nil
  
}